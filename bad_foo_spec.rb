# This basically tests that Puppet works.
describe 'foo' do
  let(:params) { :param => 'somevalue' }
  
  it do
    should contain_file('bar').with({
      :ensure  => present, 
      :owner   => root,
      :group   => root,
      :mode    => 0644,
      :content => 'somevalue'
    })
  end
end