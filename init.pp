class foo (
  $param = value,
) {

  file { 'bar':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => 0644,
    content => $param,
  }
}