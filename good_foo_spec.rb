# Better test
describe 'foo' do
  let(:params) { :param => 'somevalue' }
  
  it 'should contain the expected resources' do
    should contain_file('bar').with_content(/^somevalue$/)
  end
end